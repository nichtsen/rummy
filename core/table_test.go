package core

import (
	"testing"
	"context"
	"fmt"
	"errors"
	"time"

)


func TestDistribute01(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer func() {
		cancel()
		time.Sleep(time.Second*1)
	}() 
	req := &Entry{
		uid: 1001,
		size: 2,
		score: 20,
		res:  make(chan EntryRes),
	}
	mgr := NewTableManager(ctx)
	mgr.Start()

	mgr.Ask <- req
	res :=	<-req.res
	if !res.OK {
		t.Error("failed entry")
		return
	}
	fmt.Printf("%v \n", res.Player)
}


func TestDistribute02(t *testing.T) {
	initTable()
	ctx, cancel := context.WithCancel(context.Background())
	
	mgr := NewTableManager(ctx)
	mgr.Start()
	const cnt = 100
	uid := 1001
	
	defer func() {
		time.Sleep(time.Second*2)
		cancel() 
		str, count := mgr.Stat()
		fmt.Printf("%s", str)	
		if count != cnt {
			t.Errorf("count does not match")
		}
	}() 

	var dist = func(xuid int) {
		req := &Entry{
		uid: xuid,
		size: 2,
		score: 20,
		res:  make(chan EntryRes),
		}
		mgr.Ask <- req
		res :=  <- req.res
		if !res.OK {
			t.Error(errors.New("failed to distribute"))
			return
		}
	}

	for i:=0;i<cnt;i++ {
		go dist(uid+i)
	}
}
