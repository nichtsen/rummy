package core

import (
	"crypto/rand"
	"math/big"
	"sort"
)

type Suit uint8
// suit enum 
const (
	Spades Suit = iota
	Hearts
	Clubs
	Diamonds
	Jocker 	 
)

type Face uint8

// | 4bit | 4bit|
// | suit | face| 
type Card uint8

var Cenum = [53]Card{
	1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, //spades 
	17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, //hearts
	33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, //clubs
	49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, //diamonds
	65,                                                 //jocker
}

func (c Card) Suit() Suit {
	return Suit(uint8(c)>>4 & 0x0f)
} 

func (c Card) Face() Face {
	return Face(uint8(c) & 0x0f)
}

func ValidCard(val Card) bool {
	for _, v := range Cenum {
		if v == val {
			return true
		}
	}
	return false
} 

type GroupType int
const (
	PureSequence GroupType = iota
	Sequence
	Set
	PureJocker 
	InvalidGroup 
)


// CtoGroup: parse group types from deck, valid groups and ungrouped cards return as second and third value
func CtoGroup(cards [][]int, cutjocker int) ([]GroupType, [][]int, [][]int) {
	groups := make([]GroupType, 0, 4)
	vali := make([][]int, 0, 4)
	rem := make([][]int, 0, 4)
	for _, group := range cards {
		typ := ctoGroup(group, cutjocker)
		if typ == InvalidGroup {
			rem = append(rem, group)
			continue
		}
		groups = append(groups, typ)
		vali = append(vali, group)
		
	}
	return groups, vali, rem
} 


// ctoGroup: parse the type of a single group
func ctoGroup(group []int, cutjocker int) GroupType {
	sort.Ints(group)
	normal, jockers := splitJocker(group, cutjocker)
	if len(normal) == 0 &&  len(jockers) > 0 {
		return PureJocker
	}
	if len(group) < 3 {
		return InvalidGroup
	}
	//  judgement of set
	if func() bool {
		if len(group) < 3 {
			return false
		}
		for i:=0; i<len(group)-1; i++ {
			cardA := Card(group[i])
			cardB := Card(group[i+1])
			if group[i] == cutjocker || group[i+1] == cutjocker || cardA.Suit() == Jocker || cardB.Suit() == Jocker {
				continue
			}
			if cardA.Face() != cardB.Face() {
				return false
			}
		}
		return true
	}(){
		return Set
	}
	// judgement of sequence
	cur := normal[0]
	jockerCnt := len(jockers)
	pure := true
	if func() bool { 
		for i:=1; i<len(normal); i++ {
			if normal[i] == cur + 1 {
				cur++
				continue
			}
			if jockerCnt > 0 {
				cur++
				jockerCnt--
				pure = false
			} else {

				return false
			}
				
		}
		return true
	}() {
		if pure {
			return PureSequence
		}
		return Sequence
	}
	return InvalidGroup	
}
// splitJocker: return non-jocker cards and jockers, notes that cutjockers would also be sperated from normal cards
func splitJocker(group []int, cutjocker int) ([]int, []int) {
	var normal, jockers []int
	for _, card := range group {
		ca := Card(card)
		if card == cutjocker || ca.Suit() == Jocker  {
			jockers = append(jockers, card)
			continue
		}
		normal = append(normal, card)
	}
	return normal, jockers
}

// ValidDeclare: to judge if a decalare is valid and return its points if not the case
func ValidDeclare(cards [][]int, cutjocker int) (int, bool) {
	group, _, rem := CtoGroup(cards, cutjocker)
	if len(group) == 0 || len(rem) > 0  {
		return calcPoints(rem, cutjocker), false
	}
	rem2, ok := pureSeq(group) 
	if !ok {
		return calcPoints(rem, cutjocker), false
	}  
	if len(rem2) == 0 {
		return 0, true
	}
	if sequence(rem2) {
		return 0, true
	} 
	return calcPoints(rem, cutjocker), false
} 

func calcPoints(rem [][]int, cutjocker int) int {
	var calc = func(card Card, cutjocker int) int {
		if (card.Face() >= 11 && card.Face() <= 13) || card.Face() == 1 {
			return  10
		}  
		if card.Suit() == Jocker || int(card) == cutjocker{
			return 0
		} 
		return int(card.Face())
		
	}
	points := 0
	for _, group := range rem {
		for _, card := range group {
			points += calc(Card(card), cutjocker)
		}
	} 
	return points
}

// pureSeq: abstract the pure sequence, return false if not exits one  
func pureSeq(group []GroupType)([]GroupType, bool) {
	flag := false
	res := make([]GroupType, 0, 4)
	for _, grouptyp := range group {
		if grouptyp == PureSequence && !flag {
			flag = true
			continue
		}
		res = append(res, grouptyp)
	}
	return res, flag
}

// sequence: if a sequence exits 
func sequence(group []GroupType) bool {
	for _, grouptyp := range group {
		if grouptyp == Sequence || grouptyp == PureSequence {
			return true
		}
	}
	return false
}

func Gen() [53]Card {
	tmp := make([]Card, len(Cenum))
	_ = copy(tmp, Cenum[:])
	var res [53]Card
	for i:=53; i>=1; i-- {
		val, _ := rand.Int(rand.Reader, big.NewInt(int64(i)))
		idx := int(val.Uint64())
		res[i-1] = tmp[idx]	
		if idx == len(tmp) - 1 {
			tmp = tmp[0:i-1]
			continue
		}
		tmp = append(tmp[0:idx], tmp[idx+1:]...)
	}
	return res
}
