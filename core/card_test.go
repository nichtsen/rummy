package core

import (
	"fmt"
	"testing"
	"sort"
)

func ExampleEnum() {
	fmt.Printf("[")
	for i:=0; i<4; i++ {
		for j:=1; j<=13; j++ {
			fmt.Printf("%d, ", j+i*16)
		}
	}
	fmt.Printf("%d]", 65)
	// Output:
	// [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 65]
} 

func ExampleSuit() {
	for _, val := range Cenum {
		fmt.Printf("%d:%d ",val.Suit(), val.Face())
	}
	// Output:
	// 0:1 0:2 0:3 0:4 0:5 0:6 0:7 0:8 0:9 0:10 0:11 0:12 0:13 1:1 1:2 1:3 1:4 1:5 1:6 1:7 1:8 1:9 1:10 1:11 1:12 1:13 2:1 2:2 2:3 2:4 2:5 2:6 2:7 2:8 2:9 2:10 2:11 2:12 2:13 3:1 3:2 3:3 3:4 3:5 3:6 3:7 3:8 3:9 3:10 3:11 3:12 3:13 4:1
}

func TestGen(t *testing.T) {
	cards := Gen()
	var ica []int
	for _, ca := range cards {
		if !ValidCard(ca) {
			t.Errorf("%d is an invalid card",ca)
		}
		ica = append(ica, int(ca))		
	}
	sort.Ints(ica)
	if fmt.Sprintf("%v", ica) != fmt.Sprintf("%v", Cenum) {
		t.Errorf("dismatched cards sets after gen method")
	} 
}

var validCards = [][][]int{
	[][]int{[]int{1,2,3}, []int{17,18,19}, []int{33,34,35}, []int{49,50,51,52}},/*4 sequences */
	[][]int{[]int{1,2,3}, []int{17,18,19}, []int{33,34,35}, []int{10,26,42,58}},/*3 sequences and 1 set*/
}

var invalidCards = [][][]int {
	[][]int{[]int{1,2,3}, []int{17,18,19}, []int{33,34,35}, []int{49,50,51,54}},/*3 sequences and a invalid; points: 21 */
	[][]int{[]int{2,2}, []int{17,18,19}, []int{33,34,35}, []int{8,9,12,54}},/*2 sequences and 2 invalid; points: 37 */
}

func TestDeclare(t *testing.T) {
	for _, cards := range validCards {
		if _, ok := ValidDeclare(cards, 13); ok {
			continue
		}
		t.Errorf("declare expected to be valid: \n %v", cards)
	}
	for _, cards := range invalidCards {
		if _, ok := ValidDeclare(cards, 13); !ok {
			continue
		}
		t.Errorf("declare expected to be invalid: \n %v", cards)
	}
}

func TestPoints(t *testing.T) {
	res := []int{}
	for _, cards := range invalidCards {
		if points, ok := ValidDeclare(cards, 13); !ok {
			res = append(res, points)
		}
	}
	exp := "[21 37]"
	out := fmt.Sprintf("%v", res) 
	if out != exp {
		t.Errorf("wrong points, expected to be \n %s\n, but got: \n %s \n", exp, out)
	}
}
