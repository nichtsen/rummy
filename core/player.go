package core

import (
	"encoding/json"
	"fmt"
	"gitlab.com/nichtsen/rummy/protol"

	"github.com/aceld/zinx/ziface"
)

type Player struct {
	conn ziface.IConnection 
	Uid int
	Tid int
	Sid int
	Score int
	Table *Table
	Seat *Seat
}

func NewPlayer(conn ziface.IConnection, uid, tid, sid, score int, tab *Table, seat *Seat) *Player {
	return &Player{
		conn: conn,
		Uid: uid,
		Tid: tid,
		Sid: sid,
		Score: score,
		Table: tab,
		Seat: seat,
	}
}

func (p *Player) SendMsg(msgID int, data any) {
	//将 data 序列化
	msg, err := json.Marshal(data)
	if err != nil {
		fmt.Println("marshal msg err: ", err)
		return
	}
	//fmt.Printf("after Marshal data = %+v\n", msg)

	if p.conn == nil {
		fmt.Println("connection in player is nil")
		return
	}

	if err := p.conn.SendMsg(uint32(msgID), msg); err != nil {
		fmt.Println("Player SendMsg error !")
		return
	}

	return
}

func (p *Player) Draw(src int) {
	var card Card
	switch src {
		case FromOpen: 
			card = p.Table.OpenDeck
		case FromClose:
			card = p.Table.ClosedDeck[0]
			p.Table.ClosedDeck = p.Table.ClosedDeck[1:]
	}
	msg := &protol.MBroadcast{
		MsgID: int(protol.Broadcast),
		Type: protol.B_Draw,
		Src: src,
	}
	for _, player := range p.Table.players {
		player.SendMsg(msg.MsgID, msg) 
	} 
	dmsg := &protol.MDraw{
		MsgID: int(protol.SyncDraw),
		Card: int(card),
		Round: p.Table.game.round,
	}
	p.SendMsg(dmsg.MsgID, dmsg)
}

func (p *Player) Finish() {}
func (p *Player) Drop(src int) {}
func (p *Player) Leave(src int) {}
func (p *Player) Declare() {}
