package core

import (
	"container/ring"
	"context"
	"strings"
	"sort"
	"fmt"

	"github.com/aceld/zinx/ziface"
	"gitlab.com/nichtsen/rummy/protol"
)
const TableQuota = 100

const (
	FromOpen = iota
	FromClose 
)



func init() {
	initTable()
}

func initTable() {
	tables = make(map[int]*Table, TableQuota)
	players = make(map[int]*Player)
	for i:=1; i<TableQuota; i++ {
		seats := [6]*Seat{}
		for i:=0; i<len(seats); i++ {
			seats[i] = &Seat{}
		}
		tables[i] = &Table{
			ID: i,
			Seats: seats,
		}
	}
}


type TableStatus int
const (
	Prepare TableStatus =  iota
	Playing 
	Declaring  
)
const seatLimit = 6
type Table struct {
	ID int
	Count int	
	Status TableStatus
	Seats [6]*Seat
	OpenDeck Card
	ClosedDeck []Card
	FinishSlot Card
	game *Game
	players []*Player
}

func (t *Table) ToMsg(sid int) *protol.TableInfo {
	seat := t.Seats[sid]
	deck := make([]int, len(seat.deck))
	for _, card := range seat.deck {
		deck = append(deck, int(card))
	}
	return &protol.TableInfo{
		ID: t.ID,
		Status: protol.TableStatus(t.Status),
		OpenDeck: int(t.OpenDeck),
		ClosedDeckLength: len(t.ClosedDeck),
		FinishSlot: int(t.FinishSlot),
		ActiveSeatID: t.game.turn.Value.(int),
		Time: seat.lowerTime,
	}
}

// welcome: accept a player
func (t *Table) welcome(uid int, size int) (int, bool) {
	for idx, seat := range t.Seats {
		if !seat.occupied {
			seat.occupied = true
			seat.uid = uid
			t.Count++
			return idx, true
		}
	}
	return 0, false
}

func (t *Table) String() string {
	var b strings.Builder
	b.WriteString(fmt.Sprintf("tid: %d count: %d\n", t.ID, t.Count))
	for idx, seat := range t.Seats {
		if seat.occupied {
			b.WriteString(fmt.Sprintf("|---sid: %d, uid %d\n", idx, seat.uid))
		}
	}
	return b.String()
}

func (t *Table) stat() (string, []int) {
	var b strings.Builder
	var list []int
	b.WriteString(fmt.Sprintf("tid: %d count: %d\n", t.ID, t.Count))
	for idx, seat := range t.Seats {
		if seat.occupied {
			b.WriteString(fmt.Sprintf("|---sid: %d, uid %d\n", idx, seat.uid))
			list = append(list, seat.uid)
		}
	}
	return b.String(), list
}

// Game: game represents an on playing card game with certain players
type Game struct {
	turn *ring.Ring
	round int
}

func (t *Table) newGame() {
	cnt := 0
	turn := ring.New(t.Count) 
	for _, seat := range t.Seats{
		if seat.occupied {
			turn.Value = seat.uid
			turn = turn.Next()
			cnt++
		}
	}
	//TODO if cnt != t.count
	t.game = &Game{turn: turn}
}

type Seat struct {
	occupied bool
	uid int 
	deck []Card
	lowerTime int
}

func (s *Seat) Flush() {
	s.occupied = false
	s.uid = 0
	s.deck = make([]Card, 0, 14)
}

// tid -> table
var tables map[int]*Table
// uid -> player
var players map[int]*Player

type Entry struct {
	uid int
	size int
	score int
	res chan EntryRes 
	conn ziface.IConnection
}
func NewEntry(uid, size, score int,  res chan EntryRes, conn ziface.IConnection) *Entry{
	return &Entry{
		uid: uid,
		size: size,
		score: score,
		res: res,
		conn: conn,
	}
}
type EntryRes struct {
	Player *Player
	OK bool
}
type TableManager struct {
	ctx context.Context
	Ask chan *Entry
}
func NewTableManager(ctx context.Context) *TableManager {
	return &TableManager{
		ctx: ctx,
		Ask: make(chan *Entry, TableQuota),
	}
}

func (t *TableManager) Start() {
	go func() {
		for {
			select {
				case <-t.ctx.Done():
					fmt.Println("table manager quits")
					return
				case entry:= <- t.Ask:
					Distribute(entry)
			}
		}
	}()
}

func (t *TableManager) String() string {
	var b strings.Builder
	var sl []int
	for _, tab := range tables {
		if tab.Count > 0 {
			str, li := tab.stat()
			b.WriteString(str)
			sl = append(sl, li...)
		}
	}
	sort.Ints(sl)
	b.WriteString(fmt.Sprintf("total: %d\n uid list:\n %v", len(sl), sl))
	return b.String()
}

func (t *TableManager) Stat() (string, int) {
	var b strings.Builder
	var sl []int
	for _, tab := range tables {
		if tab.Count > 0 {
			str, li := tab.stat()
			b.WriteString(str)
			sl = append(sl, li...)
		}
	}
	sort.Ints(sl)
	b.WriteString(fmt.Sprintf("total: %d\n uid list:\n %v", len(sl), sl))
	return b.String(), len(sl)
}

func GetPlayerByPid(pid int) (*Player, bool) {
	p, ok := players[pid]
	return p, ok
} 

// Distribute: distribute an incomming player to a table seat
func Distribute(entry *Entry) {
	max, tid := -1, -1
	var ntab *Table
	for id, tab := range tables {
		if tab.Status == Prepare && tab.Count > max && tab.Count < entry.size && tab.Count < seatLimit {
			max = tab.Count
			tid = id
			ntab = tab
		}
	} 
	if tid == -1 || ntab == nil  {
		entry.res <- EntryRes{
			OK: false,
		}
		return 
	}
	sid, ok := ntab.welcome(entry.uid, entry.size)
	if !ok {
		entry.res <- EntryRes{
				OK: false,
			}
		return 
	}
	player :=  NewPlayer(entry.conn, entry.uid, tid, sid, entry.score, ntab, ntab.Seats[sid])
	players[entry.uid] = player
	ntab.players = append(ntab.players, player)
	entry.res <- EntryRes{
		OK: true,
		Player: player,
	} 
} 
