package protol

type MsgIDType int

const (
	Play MsgIDType = 1001
	LeaveTable  MsgIDType = 1002
	Emoji MsgIDType = 1003
	Broadcast MsgIDType = 200
	SyncTable MsgIDType = 201 
	SyncDraw MsgIDType = 202
)

type Action int
const (
	Draw Action = 1 <<  iota 
	Discard 
	Finish
	Drop
	Declare
)
type MPlay struct {
	MsgID int `json:"cmd"`
	Action Action `json:"action"`// draw discard finish drop declare
	Src int `json:"src"`
	Card int `json:"card"`
	Content string `json:"content"`
}

type BroadcastType int 
const (
	B_Draw BroadcastType = 1 << iota
	B_Discard   
	B_Finish
	B_Drop
	B_Declare
	B_Emoji
	B_LeaveTable 
)
type MBroadcast struct {
	MsgID int `json:"cmd"`
	Type BroadcastType `json: "type"`
	Uid  int `json: "uid"`
	Src  int `json: "src"`// 0: closedeck; 1: opendeck
	FCard int `json: "finish_card"`
	DCard int `json: "discard_card"`
	Content int `json: "content"`
	Pose *Pose `json: "pose"`
	
}

type MDraw struct {
	MsgID int `json:"cmd"`
	Card int `json:"card"`
	Round int `json:"round"`
}

type Pose struct {
	Uid int `json: "uid"`
	EmojiID int `json: "emoji_id"`
}

type UserStatus int
const (
	Await = 1 << iota
	Dropped
)
type User struct {
	ID int `json: "id"`
	Status UserStatus `json: "status"`
	Name string `json: "name"`
	Avatar string `json: "avatar"`
	LowerTime int `json: "lower_time"`
}


type MTable struct {
	MsgID int `json:"cmd"`
	// seat index -> user id
	TableInfo *TableInfo `json:"table_info"`
}

type TableStatus int
const (
	Playing = 1 << iota
	Declaring  
	Prepare
)
type TableInfo struct {
	ID int `json:"id"`
	Status TableStatus `json:"status"`
	ClosedDeckLength int `json:"closed_deck_length"`
	OpenDeck int `json:"open_deck"`
	FinishSlot int `json:"finish_slot"`
	ActiveSeatID int `json:"active_seat_id"`
	Time int `json:"time"`
	LowerTime int `json:"lower_time"`
	Deck []int `json:"deck"` 
	// seat index -> user id
	Seat map[int]*User `json:"seat"`
}
