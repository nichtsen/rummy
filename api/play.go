package api

import (
	"fmt"
	"encoding/json"
	"github.com/aceld/zinx/ziface"
	"gitlab.com/nichtsen/rummy/protol"
	"gitlab.com/nichtsen/rummy/core"
	"github.com/aceld/zinx/znet"
)

type PlayApi struct {
	znet.BaseRouter
}

func (p *PlayApi) Handle(request ziface.IRequest) {
	raw, err  := request.GetConnection().GetProperty("player")
	if err != nil {
		fmt.Println("fail to get player")
		return 
	}
	player, ok := raw.(*core.Player)
	if !ok {
		fmt.Println("fail to convert to Player from interface")
		return
	}
	msg := &protol.MPlay{}
	err = json.Unmarshal(request.GetData(), msg)
	if err != nil {
		fmt.Println("Playapi: msg Unmarshal error ", err)
		return
	}
	switch msg.Action {
		case protol.Draw:
			player.Draw(msg.Src)
		case protol.Discard:
		case protol.Finish:
		case protol.Drop:
		case protol.Declare:
		
	}
}

