package net

import (
	"testing"
)

func TestParseID(t *testing.T) {
	raws:= [][]byte{
		[]byte(`{"cmd":1001}`),
		[]byte(`{"cmd":1001, "data":"data"}`),
	}
	for _, raw := range raws {
		parseTest(raw, t)
	}
}

func parseTest(raw []byte, t *testing.T) {
	id ,err := parseID(raw)
	if err != nil {
		t.Error(err)
	}
	if id != 1001 {
		t.Errorf("id expected to be 1001, but gotten %d", id)
	}
}
