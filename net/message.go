package net

import (
	"errors"
	"encoding/json"

	"github.com/aceld/zinx/utils"
	"github.com/aceld/zinx/ziface"
	"github.com/aceld/zinx/znet"
)

var defaultHeaderLen uint32 = 8

// DataPack adaptor for json
type DataPack struct{}

// NewDataPack 封包拆包实例初始化方法
func NewDataPack() ziface.Packet {
	return &DataPack{}
}

// GetHeadLen 获取包头长度方法
func (dp *DataPack) GetHeadLen() uint32 {
	return 0 
}

//Pack just return json blob
func (dp *DataPack) Pack(msg ziface.IMessage) ([]byte, error) {
	return msg.GetData(), nil
}

//Unpack 拆包方法(解压数据)
func (dp *DataPack) Unpack(binaryData []byte) (ziface.IMessage, error) {
	id, err := parseID(binaryData)
	if err != nil {
		return nil, err
	}
	msg := znet.NewMsgPackage(uint32(id), binaryData)


	//判断dataLen的长度是否超出我们允许的最大包长度
	if utils.GlobalObject.MaxPacketSize > 0 && msg.DataLen > utils.GlobalObject.MaxPacketSize {
		return nil, errors.New("too large msg data received")
	}

	//这里只需要把head的数据拆包出来就可以了，然后再通过head的长度，再从conn读取一次数据
	return msg, nil
}

func parseID (b []byte) (int, error) {
	var msg = struct {
		ID int `json:"cmd"`
	}{}
	err := json.Unmarshal(b, &msg)
	if err != nil {
		return 0, err
	}
	return msg.ID, nil 
}
