module gitlab.com/nichtsen/rummy

go 1.18

require (
	github.com/aceld/zinx v1.0.1
	github.com/gorilla/websocket v1.5.0
)
