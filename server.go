package main 

import (
	"fmt"
	"strconv"
	"net/http"
	"net"
	"log"
	"context"
	"github.com/gorilla/websocket"

	"github.com/aceld/zinx/ziface"
	"github.com/aceld/zinx/utils"
	"gitlab.com/nichtsen/rummy/api"
	"gitlab.com/nichtsen/rummy/core"
	"gitlab.com/nichtsen/rummy/protol"
	. "gitlab.com/nichtsen/rummy/net"
	"github.com/aceld/zinx/znet"
)

type server struct {
	znet.Server
}

var handle = znet.NewMsgHandle()
var tabmgr *core.TableManager 

func OnConnectionAdd(conn ziface.IConnection) {
	raw, err  := conn.GetProperty("uid")
	if err != nil {
		fmt.Println("fail to get uid")
		return 
	}
	uid := raw.(int)

	var score, size int 	

	res := make(chan core.EntryRes)
	req := core.NewEntry(uid, size, score, res, conn)

	tabmgr.Ask <- req
	ret :=	<-res
	if !ret.OK {
		log.Println("failed entry")
		return
	}
	p := ret.Player
	conn.SetProperty("player", p)
	msg := protol.MTable {
		MsgID: int(protol.SyncTable),
		TableInfo: &protol.TableInfo{
			ID: p.Table.ID,
			Status: protol.TableStatus(p.Table.Status),
			OpenDeck: int(p.Table.OpenDeck),
		},
	}
	// broadcast
	_ = msg
}

func OnConnectionLost(conn ziface.IConnection) {

}

func main() {
	//创建服务器句柄
	s := newServer()

	//注册客户端连接建立和丢失函数
	s.SetOnConnStart(OnConnectionAdd)
	s.SetOnConnStop(OnConnectionLost)

	//注册路由
	handle.AddRouter(1001, &api.PlayApi{})
	handle.AddRouter(1002, &api.LeaveApi{})

	//table manager for cards game
	ctx, cancel := context.WithCancel(context.Background())
	tabmgr = core.NewTableManager(ctx)
	defer cancel()

	//启动服务
	s.Run()
}

func newServer() *server {
	s := znet.Server{
		Name:       utils.GlobalObject.Name,
		IPVersion:  "tcp4",
		IP:         utils.GlobalObject.Host,
		Port:       utils.GlobalObject.TCPPort,
		ConnMgr:    znet.NewConnManager(),
	}
	opt := znet.WithPacket(NewDataPack())
	opt(&s)
	return &server{Server: s}
}

var upgrader = websocket.Upgrader{
        ReadBufferSize:  1024,
        WriteBufferSize: 1024,
}

var cID uint32 = 0


func (s *server) wsHandler(w http.ResponseWriter, r *http.Request) {
        if r.Header.Get("Origin") != "http://"+r.Host {
                http.Error(w, "Origin not allowed", http.StatusForbidden)
                return
        }

        wsConn, err := upgrader.Upgrade(w, r, nil)
        if err != nil {
                fmt.Println("upgrade:", err)
                return
        }
	// adaptor
	conn := NewWSConn(wsConn)

	fmt.Println("Get conn remote addr = ", conn.RemoteAddr().String())

        
	//3.2 设置服务器最大连接控制,如果超过最大连接，那么则关闭此新的连接
        if s.ConnMgr.Len() >= utils.GlobalObject.MaxConn {
                                conn.Close()
				return
        }

       //3.3 处理该新连接请求的 业务 方法， 此时应该有 handler 和 conn是绑定的
       dealConn := NewConnection(s, conn, cID, handle)
       cID++

       //3.4 启动当前链接的处理业务
       dealConn.Start()
 
}

func (s *server) rootHandler(w http.ResponseWriter, r *http.Request) {
        content := "hello"
        fmt.Fprintf(w, "%s", content)
}




func (s *server) Start() {
	fmt.Printf("[START] Server name: %s,listenner at IP: %s, Port %d is starting\n", s.Name, s.IP, s.Port)

	//开启worker pool
	go func() {
		// 启动worker工作池机制
		handle.StartWorkerPool()
	}()

	tabmgr.Start()
}
func (s *server) Run() {
        http.HandleFunc("/ws", s.wsHandler)
        http.HandleFunc("/", s.rootHandler)
        s.Start()
	addr := net.JoinHostPort(s.IP, strconv.Itoa(s.Port))
	http.ListenAndServe(addr, nil)

}

